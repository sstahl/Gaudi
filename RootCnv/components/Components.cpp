#include "RootCnv/RootCnvSvc.h"
#include "RootCnv/RootEvtSelector.h"
#include "RootCnv/RootPerfMonSvc.h"

DECLARE_COMPONENT( Gaudi::RootCnvSvc )
DECLARE_COMPONENT( Gaudi::RootEvtSelector )
DECLARE_COMPONENT( Gaudi::RootPerfMonSvc )
